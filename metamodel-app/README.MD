# Classifer Web App

```
├── classifier-app
│   ├── src
│   │	├── main
│   │	│	├── java
│   │	│	├── resources
│   │	│	│	├── META-INF
│   │	│	│	│	├── beans.xml
│   │	│	│	│	├── persistence.xml
│   │	│	├── webapp
│   │	│	│	├── WEB-INF
│   │	│	│	│	├── web.xml
│   │	│	│	│	├── faces-config.xml
│   │	│	│	│	├── jboss-web.xml
│   │	│	│	│	├── jboss-deployment-structure.xml
│   │	│	│	│	├── keycloak.json
│   │	├── test
│   │	│	├── java
│   │	│	├── resources
│   │	│	│	├── META-INF
│   │	│	│	│	├── beans.xml
│   │	│	│	├── arquillian.xml
│   │	│	│	├── testng.xml
│   ├── pom.xml
│   ├── README.MD
```