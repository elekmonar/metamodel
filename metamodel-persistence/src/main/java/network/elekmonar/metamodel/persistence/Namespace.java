package network.elekmonar.metamodel.persistence;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Пространство имён
 * 
 * @author Vitaly Masterov
 * @since 0.1 
 *
 */
@Entity(name = "Namespace")
@Table(name = "namespace", schema = "metamodel")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Namespace implements Serializable {

	private static final long serialVersionUID = -5928437179066904521L;
	
	private Integer id;
	
    /**
     * URI
     */
    private String uri;
    
    /**
     * Полное название
     */
    private String name;
    
    /**
     * Описание
     */
    private String description;
    
    @Id
    @Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

    @Column(name = "uri", length = 128, nullable = false, unique = true)
    public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

    @Column(name = "name", length = 64, nullable = false, unique = true)	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    @Column(name = "description", length = 256, nullable = true)		
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		
	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof Namespace)) {
            return false;
        }
        Namespace obj = (Namespace)value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }
	

}