package network.elekmonar.metamodel.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.jpa.QueryHints;

import network.elekmonar.metamodel.persistence.usertype.MultiplicityUserType;
import network.elekmonar.metamodel.spec.enums.Multiplicity;

/**
 * Информационный атрибут
 *
 * @author Vitaly Masterov
 * @since 0.1
 * @see RelationshipAttribute
 * @see BaseAttribute
 * @see IntermediateAttribute
 */
@Entity(name = "InfoAttribute")
@Table(
		name = "info_attribute", 
		schema = "metamodel", 
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {"label", "type_id"}),
				@UniqueConstraint(columnNames = {"code"})
		}
)
@NamedQueries({
	@NamedQuery(
			name = "infoAttribute.all", 
			query = "from InfoAttribute",
			hints = @QueryHint(name = QueryHints.HINT_CACHEABLE, value = "false")
	),
	@NamedQuery(
			name = "infoAttribute.by.label", 
			query = "from InfoAttribute where label = :label",
			hints = @QueryHint(name = QueryHints.HINT_CACHEABLE, value = "true")			
	),
	@NamedQuery(
			name = "infoAttribute.by.code", 
			query = "from InfoAttribute where code = :code",
			hints = @QueryHint(name = QueryHints.HINT_CACHEABLE, value = "true")			
	),
	@NamedQuery(
			name = "infoAttribute.by.type", 
			query = "select attr from InfoAttribute attr where attr.type = :type",
			hints = @QueryHint(name = QueryHints.HINT_CACHEABLE, value = "false")
	)	
})
@TypeDefs({
    @TypeDef(name = "multiplicity", typeClass = MultiplicityUserType.class)
})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "discriminator", discriminatorType = DiscriminatorType.CHAR)
@DiscriminatorValue("A")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class InfoAttribute implements Serializable {

	private static final long serialVersionUID = -468649257689427881L;
	
	private Integer id;

	/**
     * Трёхсимвольный уникальный код;
     * используется в jsonb для задания ключа
     */
    private String code;

    /**
     * Метка/Литерал
     * Название переменной, применяемой в Java-классе
     */
    private String label;

    /**
     * Полное название
     */
    private String name;

    /**
     * Описание
     */
    private String description;
 
    /**
     * Тип данных аттрибута (указатель на ресурс)
     */
    private InfoResource type;
    
    /**
     * Множественность
     */
    private Multiplicity multiplicity;
 
    /**
     * Пространство имён
     */
    private Namespace namespace;
 
    @Id
    @Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

    @Column(name = "label", length = 64, nullable = false)
    public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

    @Column(name = "code", length = 3, nullable = false, unique = true)	
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
    @Column(name = "name", length = 256, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description")
    @Type(type = "text")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
 
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "type_id", nullable = false)
    public InfoResource getType() {
		return type;
	}

	public void setType(InfoResource type) {
		this.type = type;
	}
	
	@Type(type = "multiplicity")    
    @Column(name = "multiplicity", columnDefinition = "multiplicity", nullable = false)	
    public Multiplicity getMultiplicity() {
		return multiplicity;
	}

	public void setMultiplicity(Multiplicity multiplicity) {
		this.multiplicity = multiplicity;
	}
 
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "namespace_id", nullable = true)
	public Namespace getNamespace() {
		return namespace;
	}

	public void setNamespace(Namespace namespace) {
		this.namespace = namespace;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof InfoAttribute)) {
            return false;
        }
        InfoAttribute obj = (InfoAttribute) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }
	
}