package network.elekmonar.metamodel.persistence;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.QueryHints;
import org.hibernate.annotations.Type;
//import org.hibernate.envers.AuditJoinTable;
//import org.hibernate.envers.Audited;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import network.elekmonar.metamodel.persistence.usertype.FunctionalResourceTypeUserType;
import network.elekmonar.metamodel.spec.enums.FunctionalResourceType;

/**
 * Информационный ресурс
 *
 * @author Vitaly Masterov
 * @since 0.1
 * 
 */
@Entity(name = "InfoResource")
@Table(
		name = "info_resource", 
		schema = "metamodel",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {
						"label", "namespace_id"
				}),
				@UniqueConstraint(columnNames = {
						"name", "namespace_id"
				})
		} 
)
@TypeDefs({
    @TypeDef(name = "functionalResourceType", typeClass = FunctionalResourceTypeUserType.class)
})
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class InfoResource implements Serializable {

	private static final long serialVersionUID = -230197126622526469L;
	
	private Short id;
	
	private FunctionalResourceType type;
	
    /**
     * Метка (литерал)
     */
    private String label;
    
    /**
     * Метка во множественном числе
     */
    private String pluralLabel;

    /**
     * Полное название
     */
    private String name;
    
    /**
     * Описание
     */
    private String description;

    /**
     * Пространство имён
     */
    private Namespace namespace;
    
	/**
     * Атрибуты, входящие непосредственно в данный информационный ресурс
     */
    private Set<InfoAttribute> attributes;
    
    /**
     * Перечень ресурсов-"интерфейсов",
     * которым "удовлетворяет" данный ресурс.
     * По сути означает, что данный ресурс, должен включать в себя
     * атрибуты, определённые в этом перечне ресурсов-"интерфейсов".
     */
    private Set<InfoResource> satisfications;
    

    @Id
    @Column(name = "id", nullable = false)
    public Short getId() {
		return id;
	}

	public void setId(Short id) {
		this.id = id;
	}
	
	@Type(type = "functionalResourceType")    
    @Column(name = "type", columnDefinition = "functional_resource_type", nullable = false)	
	public FunctionalResourceType getType() {
		return type;
	}

	public void setType(FunctionalResourceType type) {
		this.type = type;
	}

	@Column(name = "label", length = 64, nullable = false, unique = true)
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    @Column(name = "plural_label", length = 64, nullable = true, unique = true)
    public String getPluralLabel() {
		return pluralLabel;
	}

	public void setPluralLabel(String pluralLabel) {
		this.pluralLabel = pluralLabel;
	}
 
    @Column(name = "name", length = 256, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 256, nullable = true)
    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
 
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "namespace_id", nullable = true)	
	public Namespace getNamespace() {
		return namespace;
	}

	public void setNamespace(Namespace namespace) {
		this.namespace = namespace;
	}

	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "info_resource_attributes",
            schema = "metamodel",
            joinColumns = @JoinColumn(name = "resource_id", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "attribute_id", referencedColumnName = "id", nullable = false)
    )
	/*
	@Audited(targetAuditMode = NOT_AUDITED)
	@AuditJoinTable(
			schema = "audit",
			name = "info_resource_attributes_audit"			
	)
	*/
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	public Set<InfoAttribute> getAttributes() {
		if (attributes == null) {
			attributes = new HashSet<>();
		}
		return attributes;
	}

    public void setAttributes(Set<InfoAttribute> attributes) {
		this.attributes = attributes;
	}
    
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "info_resource_satisfications",
            schema = "metamodel",
            joinColumns = @JoinColumn(name = "resource_id", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "satisfication_id", referencedColumnName = "id", nullable = false)
    )
	/*
	@Audited(targetAuditMode = NOT_AUDITED)
	@AuditJoinTable(
			schema = "audit",
			name = "info_resource_satisfications_audit"			
	)
	*/
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)	
	public Set<InfoResource> getSatisfications() {
		if (satisfications == null) {
			satisfications = new HashSet<>();
		}
		
		return satisfications;
	}

	public void setSatisfications(Set<InfoResource> satisfications) {
		this.satisfications = satisfications;
	}    
	
	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof InfoResource)) {
            return false;
        }
        InfoResource obj = (InfoResource) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}