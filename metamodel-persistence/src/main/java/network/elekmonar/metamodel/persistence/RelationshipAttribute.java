package network.elekmonar.metamodel.persistence;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 * Сущность, унаследованная от {@link InfoAttribute}
 * и определяющая описание атрибута - связи между узлами.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see InfoAttribute
 * @see BaseAttribute
 * @see IntermediateAttribute
 *
 */
@Entity(name = "RelationshipAttribute")
@DiscriminatorValue("R")
public class RelationshipAttribute extends InfoAttribute {

	private static final long serialVersionUID = 6970099821781752562L;
	
	/**
	 * Указывает на то, является ли ассоциация композитной.
	 * Другими словами, узлы на другом конце, которые прекреплены
	 * посредством данной связи управяляются узлом, от которого связь идёт,
	 * и не могут существовать самостоятельно.
	 */
	private Boolean composite;
	
	/**
	 * Признак того, является ли связь упорядоченный,
	 * т.е. выставлены ли узлы данных в пределах
	 * данной связи в определённом порядке 
	 * (по полю {@literal position}).
	 */
	private Boolean ordered;
	
	/**
	 * В случае упорядоченной связи (но не по индексу)
	 * указывает на атрибут типа "свойство",
	 * по которому происходит сортировка
	 */
	private PropertyAttribute orderAttribute;
	
	@Column(name = "composite", nullable = true)
	public Boolean getComposite() {
		return composite;
	}

	public void setComposite(Boolean composite) {
		this.composite = composite;
	}

	@Column(name = "ordered", nullable = true)
	public Boolean getOrdered() {
		return ordered;
	}

	public void setOrdered(Boolean ordered) {
		this.ordered = ordered;
	}
		
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "order_attribute_id", nullable = true)
	public PropertyAttribute getOrderAttribute() {
		return orderAttribute;
	}

	public void setOrderAttribute(PropertyAttribute orderAttribute) {
		this.orderAttribute = orderAttribute;
	}
	
	@Transient
	public Boolean getCompositeFeature() {
		return composite;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof RelationshipAttribute)) {
            return false;
        }
        RelationshipAttribute obj = (RelationshipAttribute) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }	
	
}