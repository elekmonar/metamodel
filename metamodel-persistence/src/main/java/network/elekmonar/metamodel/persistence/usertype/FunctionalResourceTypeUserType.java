package network.elekmonar.metamodel.persistence.usertype;

import network.elekmonar.elefante.hibernate.usertype.EnumUserType;
import network.elekmonar.metamodel.spec.enums.FunctionalResourceType;

public class FunctionalResourceTypeUserType extends EnumUserType<FunctionalResourceType> {

	public FunctionalResourceTypeUserType(Class<FunctionalResourceType> clazz) {
		super(clazz);
	}

	public FunctionalResourceTypeUserType() {
		super(FunctionalResourceType.class);
	}
	
}