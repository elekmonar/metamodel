package network.elekmonar.metamodel.persistence.usertype;

import network.elekmonar.elefante.hibernate.usertype.EnumUserType;
import network.elekmonar.metamodel.spec.enums.Multiplicity;

public class MultiplicityUserType extends EnumUserType<Multiplicity> {

	public MultiplicityUserType(Class<Multiplicity> clazz) {
		super(clazz);
	}

	public MultiplicityUserType() {
		super(Multiplicity.class);
	}

}