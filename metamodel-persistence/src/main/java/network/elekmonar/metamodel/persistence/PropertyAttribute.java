package network.elekmonar.metamodel.persistence;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Описание атрибута, соответствующего свойству 
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see InfoAttribute
 * @see RelationshipAttribute 
 */
@Entity(name = "PropertyAttribute")
@DiscriminatorValue("P")
public class PropertyAttribute extends InfoAttribute {

	private static final long serialVersionUID = -6461516914838124842L;
	
	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof PropertyAttribute)) {
            return false;
        }
        PropertyAttribute obj = (PropertyAttribute) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

}