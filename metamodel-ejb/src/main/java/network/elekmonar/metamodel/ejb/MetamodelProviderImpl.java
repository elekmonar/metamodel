package network.elekmonar.metamodel.ejb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.QueryHints;

import network.elekmonar.metamodel.persistence.InfoResource;
import network.elekmonar.metamodel.persistence.InfoResource_;
import network.elekmonar.metamodel.persistence.Namespace;
import network.elekmonar.modular.spec.IdName;
import network.elekmonar.modular.spec.IdNameFactory;
import network.elekmonar.modular.spec.metamodel.MetamodelProvider;

@Stateless
public class MetamodelProviderImpl implements MetamodelProvider, Serializable {

	private static final long serialVersionUID = 2719627444258751525L;
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private IdNameFactory idNameFactory;

	@Override
	public Short getResourceIdByLabel(String label) {
		CriteriaBuilder cBuilder = em.getCriteriaBuilder();
		CriteriaQuery<InfoResource> cQuery = cBuilder.createQuery(InfoResource.class);
		Root<InfoResource> cRoot = cQuery.from(InfoResource.class);
		Predicate predicate = cBuilder.equal(cRoot.get(InfoResource_.label), StringUtils.capitalize(label));
		cQuery.where(predicate);
		InfoResource resource = em.createQuery(cQuery)
			.setHint(QueryHints.CACHEABLE, Boolean.TRUE)
			.getSingleResult();
		
		return resource.getId();
	}

	@Override
	public String getResourceLabelById(Short id) {
		InfoResource resource = em.find(InfoResource.class, id);
		return resource.getLabel();
	}
	
	@Override
	public List<IdName<Integer>> getNamespaces() {
		CriteriaBuilder cBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Namespace> cQuery = cBuilder.createQuery(Namespace.class);
		cQuery.from(Namespace.class);
		List<Namespace> list = em.createQuery(cQuery)
				.setHint(QueryHints.CACHEABLE, Boolean.TRUE)
				.getResultList();
		
		List<IdName<Integer>> result = new ArrayList<>();
		for (Namespace namespace : list) {
			IdName<Integer> bean = idNameFactory.newInstance(Integer.class);
			bean.setId(namespace.getId());
			bean.setName(namespace.getName());
			result.add(bean);
		}
		
		return result;
	}

	@Override
	public List<IdName<Short>> getResources(Integer namespaceId) {
		Namespace namespace = em.find(Namespace.class, namespaceId);
		
		CriteriaBuilder cBuilder = em.getCriteriaBuilder();
		CriteriaQuery<InfoResource> cQuery = cBuilder.createQuery(InfoResource.class);
		Root<InfoResource> cRoot = cQuery.from(InfoResource.class);
		Predicate predicate = cBuilder.equal(cRoot.get(InfoResource_.namespace), namespace);
		cQuery.where(predicate);
		List<InfoResource> list = em.createQuery(cQuery)
			.setHint(QueryHints.CACHEABLE, Boolean.TRUE)
			.getResultList();
		
		List<IdName<Short>> result = new ArrayList<>();
		for (InfoResource resource : list) {
			IdName<Short> bean = idNameFactory.newInstance(Short.class);
			bean.setId(resource.getId());
			bean.setName(resource.getName());
			result.add(bean);
		}
		
		return result;		
	}

	@Override
	public IdName<Short> getResource(Short id) {
		InfoResource resource = em.find(InfoResource.class, id);
		IdName<Short> bean = idNameFactory.newInstance(Short.class);
		bean.setId(resource.getId());
		bean.setName(resource.getName());

		return bean;
	}

}