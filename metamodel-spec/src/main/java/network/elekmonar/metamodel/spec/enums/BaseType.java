package network.elekmonar.metamodel.spec.enums;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Перечисление, в котормо сопоставлены взятые из БД (таблица ресурсов)
 * базовые типы данных.
 * Здесь представленя информация о label-ах геоатрибутов, а также их
 * идентификаторах в соответствии с тем, как это представлено в БД
 * в таблице info_attribute.
 * @author Vitaly Masterov
 *
 */
public enum BaseType {

	CHAR(1, "Char", "Символ", Character.class),

	STRING(2, "String", "Строка", String.class),

	NUMBER(3, "Number", "Базовый числовой тип", Number.class),

	SHORT(4, "Short", "Целочисленный Short", Short.class),
	
	INTEGER(5, "Integer", "Целочисленный Integer", Integer.class),

	LONG(6, "Long", "Целочисленный Long", Long.class),

	FLOAT(7, "Float", "Целочисленный Float", Float.class),

	DOUBLE(8, "Double", "Целочисленный Double", Double.class),

	BOOLEAN(9, "Boolean", "Логический", Boolean.class),

	DATE(10, "Date", "Дата", LocalDate.class),

	TIME(11, "Time", "Время", OffsetTime.class),
	
	ZONED_TIME(12, "ZonedTime", "Время", LocalTime.class),

	DATE_TIME(13, "DateTime", "Дата и время", LocalDateTime.class),
	
	ZONED_DATE_TIME(14, "ZonedDateTime", "Дата и время", OffsetDateTime.class),
	
	TIME_FRAME(15, "TimeFrame", "Временной интервал", Period.class),

	GUID(16, "UUID", "Уникальный идентификатор формата UUID", UUID.class);

	private Integer id;

	private String label;

	private String name;

	private Class<?> javaType;

	private static Set<BaseType> numberTypes;

	private static Set<BaseType> xTimeTypes;

	private static Map<Integer, BaseType> byIds;

	private static Map<String, BaseType> byLabels;

	private static Map<Class<?>, BaseType> byClasses;

	static {
		byIds = new HashMap<>();
		byLabels = new HashMap<>();
		byClasses = new HashMap<>();
		for (BaseType bType : BaseType.values()) {
			byIds.put(bType.getId(), bType);
			byLabels.put(bType.getLabel(), bType);
			byClasses.put(bType.getJavaType(), bType);
		}

		numberTypes = new HashSet<>();
		numberTypes.add(INTEGER);
		numberTypes.add(LONG);
		numberTypes.add(FLOAT);
		numberTypes.add(DOUBLE);

		xTimeTypes = new HashSet<>();
		xTimeTypes.add(DATE);
		xTimeTypes.add(TIME);
		xTimeTypes.add(DATE_TIME);
	}

	BaseType(Integer id, String label, String name, Class<?> javaType) {
		this.id = id;
		this.label = label;
		this.name = name;
		this.javaType = javaType;
	}

	public Integer getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public String getName() {
		return name;
	}

	public Class<?> getJavaType() {
		return javaType;
	}

	public static BaseType fromId(Integer id) {
		return byIds.get(id);
	}

	public static BaseType fromLabel(String label) {
		return byLabels.get(label);
	}

	public static BaseType fromJavaType(Class<?> javaType) {
		return byClasses.get(javaType);
	}

	/**
	 * Определяет является ли такой атрибут числовым типом.
	 * @return
	 */
	public boolean isNumberType() {
		return numberTypes.contains(this);
	}

	/**
	 * Определяет является ли такой атрибут типом
	 * либо DATE, либо TIME, либо DATE_TIME
	 * @return
	 */
	public boolean isXTimeType() {
		return xTimeTypes.contains(this);
	}
	
}