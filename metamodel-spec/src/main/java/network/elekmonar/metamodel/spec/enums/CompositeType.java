package network.elekmonar.metamodel.spec.enums;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Перечень композитных (составных) типов данных,
 * представленных в базе данных.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see BaseType
 *
 */
public enum CompositeType {

	ARTIFACT(0, "Artifact", "Файл"),
	
	GEO(0, "Geo", "Базовый географический тип"),
	
	GEO_POINT(0, "Point", "Гео-точка"),
	
	GEO_LINE(0, "Line", "Гео-линия"),
	
	GEO_POLYGON(0, "Polygon", "Гео-полигон"),
	
	GEO_MULTIPOLYGON(0, "Multipolygon", "Гео-мультиполигон"),
		
	BIG_TEXT(0, "BigText", "Big Text Data"),
	
	RANGE(0, "Range", "Диапазон каких-либо данных с заданием нижней и верхней границы диапазона"),
	
	CHAR_RANGE(0, "CharRange", "Символьный диапазон"),
	
	DATE_RANGE(0, "DateRange", "Диапазон дат"),
	
	TIME_RANGE(0, "TimeRange", "Диапазон времени"),
	
	DATE_TIME_RANGE(0, "DateTimeRange", "Диапазон даты и времени"),
	
	NUMBER_RANGE(0, "NumberRange", "Числовой диапазон"),
	
	SHORT_RANGE(0, "ShortRange", "Числовой диапазон типа Short"),
	
	INTEGER_RANGE(0, "NumberRange", "Числовой диапазон типа Integer"),
	
	LONG_RANGE(0, "NumberRange", "Числовой диапазон типа Long"),
	
	FLOAT_RANGE(0, "NumberRange", "Числовой диапазон типа Float"),
	
	DOUBLE_RANGE(0, "NumberRange", "Числовой диапазон типа Double");
	
	private Integer id;
	
	private String label;
	
	private String name;
	
	private static Set<CompositeType> geoTypes;

	private static Map<Integer, CompositeType> byIds;
	
	private static Map<String, CompositeType> byLabels;
	
	static {
		byIds = new HashMap<>();
		byLabels = new HashMap<>();
		for (CompositeType iType : CompositeType.values()) {
			byIds.put(iType.getId(), iType);
			byLabels.put(iType.getLabel(), iType);
		}		
		
		geoTypes = new HashSet<>();
		geoTypes.add(GEO_POINT);
		geoTypes.add(GEO_LINE);
		geoTypes.add(GEO_POLYGON);
		geoTypes.add(GEO_MULTIPOLYGON);
	}
	
	CompositeType(Integer id, String label, String name) {
		this.id = id;
		this.label = label;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}
		
	public String getName() {
		return name;
	}

	public static CompositeType fromId(UUID id) {
		return byIds.get(id);
	}

	public static CompositeType fromLabel(String label) {
		return byLabels.get(label);
	}
	
	/**
	 * Определяет является ли такой атрибут геотипом.
	 * @return
	 */
	public boolean isGeoType() {
		return geoTypes.contains(this);
	}
		
	/**
	 * Определяет, является тип атрибута артефактом.
	 * @return true or false
	 */
	public boolean isArtifact() {
		if (this.equals(ARTIFACT)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Определяет, является тип атрибута "большим текстом".
	 * @return true or false
	 */
	public boolean isBigText() {
		if (this.equals(BIG_TEXT)) {
			return true;
		} else {
			return false;
		}
	}
	
}