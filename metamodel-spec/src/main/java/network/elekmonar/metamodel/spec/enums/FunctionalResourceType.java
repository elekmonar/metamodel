package network.elekmonar.metamodel.spec.enums;

/**
 * Функциональный тип ресурса
 * 
 * @author Vitaly Masterov
 * @see InfoResource
 * @since 0.1
 *
 */
public enum FunctionalResourceType {

    /**
     * Базовый тип данных (такой как integer, float, string, geo и т.д.)
     */
    BASE("Базовый"),

    /**
     * Составной(композитный) тип (что-то между BASE и ENTITY),
     * например Artifact, GeoLocation или Range.
     */
    COMPOSITE("Составной"),

    /**
     * Интерфейс.
     * У такого типа ресурса нет экземпляров, он лишь задаёт набор атрибутов,
     * которые должен содержать ресурс, имплементирующий такой интерфейс.
     */
    INTERFACE("Интерфейс"),
    
    /**
     * Тип ресурса вида ссылка на другой "ресурс"
     */
    ENTITY("Сущность"),

    /**
     * Ресурс, атрибуты которого вычисляются на основе какого-либо алгоритма.
     */
    CALC("Вычисляемый");

    private String name;

    FunctionalResourceType(String name) {
            this.name = name;
    }

    public String getName() {
            return name;
    }

    public boolean isInterface() {
            if (this.equals(INTERFACE)) {
                    return true;
            } else {
                    return false;
            }
    }

    public boolean isBase() {
            if (this.equals(BASE)) {
                    return true;
            } else {
                    return false;
            }
    }

    public boolean isIntermediate() {
            if (this.equals(COMPOSITE)) {
                    return true;
            } else {
                    return false;
            }
    }

    public boolean isEntity() {
            if (this.equals(ENTITY)) {
                    return true;
            } else {
                    return false;
            }
    }

    public boolean isCalc() {
            if (this.equals(CALC)) {
                    return true;
            } else {
                    return false;
            }
    }
	
}