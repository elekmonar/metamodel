package network.elekmonar.metamodel.spec.exceptions;

/**
 * Это исключение говорит о том, что не удалось распознать(определить)
 * формат файла по его содержимому или расширению.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class UndefinedFileTypeException extends RuntimeException {

	private static final long serialVersionUID = -9184547436758024867L;
	
	private String sourcePath;
	
	public UndefinedFileTypeException(String sourcePath) {
		this.sourcePath = sourcePath;
	}
	
	public String getSourcePath() {
		return sourcePath;
	}

}