package network.elekmonar.metamodel.spec.enums;

/**
 * Множественность структуры данных
 * Может быть либо единичным значением (или ассоциация to-one),
 * либо массивом (коллекцией) (или ассоциация to-many)
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public enum Multiplicity {

	SINGLE,
	
	MULTI;
	
	Multiplicity() {
		
	}
	
}